﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AndroCockJavaDecompiler
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /*
             * apkdexex: http://ibotpeaches.github.io/Apktool
             * apkdexjar: https://github.com/pxb1988/dex2jar 
             * fern: https://github.com/fesh0r/fernflower
            */
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new AndroCockMain());
        }
    }
}
