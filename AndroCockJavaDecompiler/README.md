# AndroCock Java Decompiler

This project is a reaction on the [AndroChef Java Decompiler](http://www.neshkov.com/ac_decompiler.html) program, that charges money for embedding some the following open source projects:

- [APKTool](http://ibotpeaches.github.io/Apktool)
- [dex2jar](https://github.com/pxb1988/dex2jar)
- [FernFlower](https://github.com/fesh0r/fernflower)

This is a project that has exactly the same functionality as AndroChef's program, but now for free!